package ru.t1.artamonov.tm.exception.user;

public final class RoleEmptyException extends AbstractUserException {

    public RoleEmptyException() {
        super("Error! UserDTO's role is empty...");
    }

}
