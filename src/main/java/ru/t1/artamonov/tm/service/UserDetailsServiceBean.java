package ru.t1.artamonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.enumerated.RoleType;
import ru.t1.artamonov.tm.model.CustomUser;
import ru.t1.artamonov.tm.model.Role;
import ru.t1.artamonov.tm.model.User;
import ru.t1.artamonov.tm.repository.UserRepository;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @NotNull
    @Autowired(required = false)
    private PasswordEncoder passwordEncoder;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Override
    public UserDetails loadUserByUsername(@NotNull final String username) throws UsernameNotFoundException {

        @Nullable final User user = userRepository.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);

        @NotNull final List<Role> userRoles = user.getRoles();
        @NotNull List<String> roles = new ArrayList<>();
        for (@NotNull final Role role : userRoles) roles.add(role.toString());

        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

    private void initUser(@NotNull final String login, @NotNull final String password, @NotNull final RoleType roleType) {
        @Nullable final User user = userRepository.findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMINISTRATOR);
        initUser("test", "test", RoleType.USER);
    }

    @Transactional
    public void createUser(final String login, final String password, final RoleType roleType) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        final String passwordHash = passwordEncoder.encode(password);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

}
