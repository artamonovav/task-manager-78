package ru.t1.artamonov.tm.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.artamonov.tm.api.service.IProjectService;
import ru.t1.artamonov.tm.api.service.ITaskService;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.model.CustomUser;
import ru.t1.artamonov.tm.model.Project;
import ru.t1.artamonov.tm.model.Task;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/task/create")
    public String create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        taskService.addByUserId(user.getUserId(), new Task("New Task " + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.deleteByIdAndUserId(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute Task task, BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.updateByUserId(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        final Task task = taskService.findByUserIdAndId(user.getUserId(), id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects(user.getUserId()));
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    @Nullable
    public Status[] getStatuses() {
        return Status.values();
    }

    @Nullable
    private Collection<Project> getProjects(@Nullable final String userId) {
        return projectService.findAllByUserId(userId);
    }

}
